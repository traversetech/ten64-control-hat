# Remote System Control hat for Traverse Ten64
![Image of control shield on top of NPi-MX6ULL](ten64_control_shield.jpg)

![Image of control shield in action](ten64_remote_control.jpg)

This is a remote control shield for the Traverse Ten64. It's primary use is for bare metal CI/CD
and remote system access (system console, UART and I2C), connecting to the [control header](https://ten64doc.traverse.com.au/hardware/control-header/)
on the Ten64 board.

Up two two systems can be controlled from the one host system, subject to availability of UARTs and GPIOs.

The baseboard itself is from NilsMinor's [Raspberry-Pi-3-Altium-Template](https://github.com/NilsMinor/Raspberry-Pi-3-Altium-Template).

## Signal routing
![RPI connector schematic](1064_hat_connectors.png)

At the time we designed this - before the RPi 4 came out with [multiple UARTs](https://www.raspberrypi.org/documentation/configuration/uart.md), the
Raspberry Pi only had one UART - the system console UART. For the RPi 3 and earlier boards, you are better off connecting to the USB-C console on the Ten64.

You can disconnect UART pins by removing the jumbers between the RPi and target board connector.

We use the [seeed studio NPI-MX6ULL boards](https://www.seeedstudio.com/NPi-i-MX6ULL-Dev-Board-Industrial-Grade-Linux-SBC-NAND-Version-p-4220.html).


|Signal           |RPI        |NPI                       |
|-----------------|-----------|--------------------------|
|UART0_TX         |(SYSTEM TX)|UART3_TX_DATA / GPIO1_24  |
|UART0_RX         |(SYSTEM RX)|UART3_RX_DATA / GPIO1_25  |
|TARGET1_PWR_ACT  |GPIO23     |CSI_HSYNC / GPIO1_09      |
|TARGET1_RST_ACT  |GPIO24     |CSI_VSYNC / GPIO1_08      |
|TARGET1_RECOV_ACT|GPIO19     |CSI_DATA4 / GPIO4_25      |
|TARGET2_PWR_ACT  |GPIO20     |CSI_DATA01 / GPIO4_22     |
|TARGET2_RST_ACT  |GPIO21     |CSI_DATA0 / GPIO4_21      |
|TARGET2_RECOV_ACT|GPIO26     |CSI_DATA2 / GPIO4_23      |
|I2C_SDA          |I2C_SDA    |UART5_RX_DATA / I2C2_SDA  |
|I2C_SCL          |I2C_SCL    |UART5_TX_DATA / I2C2_SCL  |

Note: access to the target I2C is muxed through a PCA9540 multiplexer.

## Software

* See [openwrt-bmc-feed](https://gitlab.com/matt_traverse/openwrt-bmc-feed) for the control side packages

(More info coming on NPi board images)

## TODO for next revision
* Support the alt mode UARTs on the Raspberry Pi 4.
* Change the PCA9540 mux footprint to something more hobbyist friendly - RevA PCB uses the same XSON8 as the Ten64.